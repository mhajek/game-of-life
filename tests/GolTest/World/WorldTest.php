<?php
namespace GolTest\World;

use Gol\World\World;
use Gol\World\Organism;
use Exception;
use RuntimeException;
use PHPUnit\Framework\TestCase;

class WorldTest extends TestCase
{

    public function testAddOrganism(): void
    {
        $organism = new Organism('dog');

        $world = new World(2, 2);
        $world->addOrganismToPosition(1, 1, $organism);

        $object = $world->getOrganismByPosition(1, 1);

        self::assertEquals($organism, $object);
    }

    public function testAddOutOfRange(): void
    {
        $organism = new Organism('dog');

        $world = new World(2, 2);
        try {
            $world->addOrganismToPosition(3, 2, $organism);
        }
        catch (Exception $e) {
            self::assertEquals(RuntimeException::class, get_class($e));
            self::assertEquals('Coordinates [3, 2] are out of range; [2, 2]', $e->getMessage());
        }
    }

    public function testGetNullOrganism(): void
    {
        $world = new World(2, 2);
        $null = $world->getOrganismByPosition(1, 1);

        self::assertNull($null);
    }

    public function testGetOrganismsAroundPosition(): void
    {
        $world = new World(5, 5);
        $world->addOrganismToPosition(2, 2, new Organism('dog1'));
        $world->addOrganismToPosition(2, 3, new Organism('dog2'));
        $world->addOrganismToPosition(2, 4, new Organism('dog3'));
        $world->addOrganismToPosition(3, 2, new Organism('mouse1'));
        $world->addOrganismToPosition(3, 4, new Organism('mouse3'));
        $world->addOrganismToPosition(4, 2, new Organism('cat1'));
        $world->addOrganismToPosition(4, 3, new Organism('cat2'));
        $world->addOrganismToPosition(4, 4, new Organism('cat3'));

        $array1 = $world->getOrganismsAroundPosition(3, 3);
        self::assertEquals('dog1', $array1[1]->getType());
        self::assertEquals('dog2', $array1[2]->getType());
        self::assertEquals('dog3', $array1[3]->getType());
        self::assertEquals('mouse1', $array1[4]->getType());
        self::assertArrayNotHasKey(5, $array1);
        self::assertEquals('mouse3', $array1[6]->getType());
        self::assertEquals('cat1', $array1[7]->getType());
        self::assertEquals('cat2', $array1[8]->getType());
        self::assertEquals('cat3', $array1[9]->getType());

        $array2 = $world->getOrganismsAroundPosition(1, 1);
        self::assertEquals('dog1', $array2[9]->getType());
    }
}
