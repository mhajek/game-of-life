<?php
namespace BrandEmbassyTest\Game;

use BrandEmbassy\Game\Transition;
use Gol\Game\Game;
use Gol\World\God;
use Gol\World\Organism;
use Gol\World\World;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    public function testGameLessThanTwo(): void
    {
        $o1 = new Organism('lajka');

        $world = new World(3, 3);
        $world->addOrganismToPosition(2, 2, $o1);
        $world->addOrganismToPosition(1, 1, $o1);

        $game = new Game(new God(), new Transition(), 1); // TODO: mock
        $newWorld = $game->run($world);

        self::assertNull($newWorld->getOrganismByPosition(1, 1));
        self::assertNull($newWorld->getOrganismByPosition(1, 2));
        self::assertNull($newWorld->getOrganismByPosition(1, 3));
        self::assertNull($newWorld->getOrganismByPosition(2, 1));
        self::assertNull($newWorld->getOrganismByPosition(2, 2));
        self::assertNull($newWorld->getOrganismByPosition(2, 3));
        self::assertNull($newWorld->getOrganismByPosition(3, 1));
        self::assertNull($newWorld->getOrganismByPosition(3, 2));
        self::assertNull($newWorld->getOrganismByPosition(3, 3));
    }

    public function testGameBornNewOrganism(): void
    {
        $o1 = new Organism('lajka');

        $world = new World(3, 3);
        $world->addOrganismToPosition(1, 1, $o1);
        $world->addOrganismToPosition(1, 3, $o1);
        $world->addOrganismToPosition(3, 3, $o1);

        $game = new Game(new God(), new Transition(), 1);
        $newWorld = $game->run($world);

        self::assertNull($newWorld->getOrganismByPosition(1, 1));
        self::assertNull($newWorld->getOrganismByPosition(1, 2));
        self::assertNull($newWorld->getOrganismByPosition(1, 3));
        self::assertNull($newWorld->getOrganismByPosition(2, 1));
        self::assertSame(spl_object_hash($o1), spl_object_hash($newWorld->getOrganismByPosition(2, 2)));
        self::assertNull($newWorld->getOrganismByPosition(2, 3));
        self::assertNull($newWorld->getOrganismByPosition(3, 1));
        self::assertNull($newWorld->getOrganismByPosition(3, 2));
        self::assertNull($newWorld->getOrganismByPosition(3, 3));
    }

    /**
     * @test
     */
    public function game(): void
    {
        $o1 = new Organism('pony');

        $world = new World(3, 3);
        $world->addOrganismToPosition(1, 1, $o1);
        $world->addOrganismToPosition(1, 3, $o1);
        $world->addOrganismToPosition(3, 3, $o1);

        $game = new Game(new God(), new Transition(), 2);
        $newWorld = $game->run($world);

        $this->assertNotSame(spl_object_hash($world), spl_object_hash($newWorld));
        $this->assertNull($newWorld->getOrganismByPosition(1, 1));
        $this->assertNull($newWorld->getOrganismByPosition(1, 2));
        $this->assertNull($newWorld->getOrganismByPosition(1, 3));
        $this->assertNull($newWorld->getOrganismByPosition(2, 1));
        $this->assertNull($newWorld->getOrganismByPosition(2, 2));
        $this->assertNull($newWorld->getOrganismByPosition(2, 3));
        $this->assertNull($newWorld->getOrganismByPosition(3, 1));
        $this->assertNull($newWorld->getOrganismByPosition(3, 2));
        $this->assertNull($newWorld->getOrganismByPosition(3, 3));
    }

}
