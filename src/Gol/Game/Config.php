<?php
namespace Gol\Game;

class Config
{
    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var int
     */
    private $iterations;

    /**
     * @var array ['i' : int, 'j' : int, 'type' : string]
     */
    private $organisms;

    public function __construct(int $width, int $height, int $iterations, array $organisms)
    {
        $this->width = $width;
        $this->height = $height;
        $this->iterations = $iterations;
        $this->organisms = $organisms;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getIterations(): int
    {
        return $this->iterations;
    }

    public function getOrganisms(): array
    {
        return $this->organisms;
    }

}
