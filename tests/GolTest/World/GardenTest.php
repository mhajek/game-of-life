<?php
namespace GolTest\World;

use Gol\World\Organism;
use Gol\World\Garden;
use PHPUnit\Framework\TestCase;

class GardenTest extends TestCase
{
    /**
     * @var Garden
     */
    private $garden;

    public function setUp(): void
    {
        parent::setUp();

        $this->garden = new Garden();
    }

    public function testBasic(): void
    {
        $zeman = new Organism('zeman');

        $garden = $this->garden;
        $garden->addOrganism(new Organism('dog'));
        $garden->addOrganism(new Organism('cat'));
        $garden->addOrganism($zeman);

        $object = $garden->getOrganism('zeman');

        self::assertSame(spl_object_hash($zeman), spl_object_hash($object));
    }

    public function testHasNotOrganism(): void
    {
        self::assertFalse($this->garden->hasOrganism(new Organism('zeman')));
    }

    public function testHasOrganism(): void
    {
        $this->garden->addOrganism(new Organism('foo'));

        self::assertTrue($this->garden->hasOrganism(new Organism('foo')));
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Organism "foo" already exists
     */
    public function testAddOrganismAlreadyExists(): void
    {
        $this->garden->addOrganism(new Organism('foo'));
        $this->garden->addOrganism(new Organism('foo'));
    }

}
