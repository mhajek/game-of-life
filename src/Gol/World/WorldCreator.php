<?php
namespace Gol\World;

interface WorldCreator
{
    public function createWorld(int $height, int $width): World;
}
