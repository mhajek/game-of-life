<?php
namespace Gol\Game;

use Gol\World\World;
use Gol\World\WorldCreator;
use InvalidArgumentException;

class Game
{
    /**
     * @var WorldCreator
     */
    private $creator;

    /**
     * @var CellTransition
     */
    private $transition;

    /**
     * @var int
     */
    private $iterations;

    public function __construct(WorldCreator $creator, CellTransition $transition, int $iterations)
    {
        if ($iterations < 1) {
            throw new InvalidArgumentException(sprintf('Invalid iterations; %s given', $iterations));
        }

        $this->creator = $creator;
        $this->transition = $transition;
        $this->iterations = $iterations;
    }

    private function applyTransition(CellTransition $transition, World $world): World
    {
        $height = $world->getHeight();
        $width = $world->getWidth();

        $newWorld = $this->creator->createWorld($height, $width);

        for ($i = 2; $i < $height; $i++) {
            for ($j = 2; $j < $width; $j++) {
                $newOrganism = $transition->apply($world, $i, $j);
                if ($newOrganism) {
                    $newWorld->addOrganismToPosition($i, $j, $newOrganism);
                }
            }
        }

        return $newWorld;
    }

    public function run(World $world): World
    {
        $i = 0;
        while ($i < $this->iterations) { // TODO: break if world is empty?
            $world = $this->applyTransition($this->transition, $world);
            $world->setYear(++$i);
        }

        return $world;
    }
}
