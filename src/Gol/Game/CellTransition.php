<?php
namespace Gol\Game;

use Gol\World\Organism;
use Gol\World\World;

interface CellTransition
{
    public function apply(World $world, int $i, int $j): ?Organism;
}
