<?php
namespace BrandEmbassy\Game;

use Gol\Game\CellTransition;
use Gol\World\Organism;
use Gol\World\World;

class Transition implements CellTransition
{

    public function apply(World $world, int $i, int $j): ?Organism
    {
        $mainOrganism = $world->getOrganismByPosition($i, $j);
        $around = $world->getOrganismsAroundPosition($i, $j);

        $tempTypes = [];
        $tempOrganisms = [];

        foreach ($around as $organism) {
            if ($organism === null) {
                continue;
            }

            $type = $organism->getType();
            if (!isset($tempTypes[$type])) {
                $tempTypes[$type] = 1;
                $tempOrganisms[$type] = $organism;
                continue;
            }

            $tempTypes[$type]++;
        }

        if ($mainOrganism === null) { // Born new organism
            $newOrganisms = [];
            foreach ($tempTypes as $type => $count) {
                if ($count === 3) {
                    $newOrganisms[] = $type;
                }
            }

            if ($newOrganisms) {
                $key = array_rand($newOrganisms);
                $newOrganismType = $newOrganisms[$key];

                return $tempOrganisms[$newOrganismType];
            }
        } elseif (isset($tempTypes[$mainOrganism->getType()])) {
            $count = $tempTypes[$mainOrganism->getType()];

            if ($count === 2 || $count === 3) { // Survive
                return $mainOrganism;
            }
        }

        // $count == 1 : Isolation
        // $count >= 4 : Die

        return null;
    }
}
