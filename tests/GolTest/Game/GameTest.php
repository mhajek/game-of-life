<?php
namespace GolTest\Game;

use Gol\Game\CellTransition;
use Gol\Game\Game;
use Gol\World\God;
use Gol\World\Organism;
use Gol\World\World;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

class GameTest extends TestCase
{
    /**
     * @return Organism
     */
    private function getNewOrganism(): Organism
    {
        return new Organism('dog');
    }

    /**
     * @return World
     */
    private function getNewWorld(): World
    {
        return new World(5, 5);
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject|CellTransition
     */
    private function createCellTransitionMock()
    {
        $builder = $this->getMockBuilder(CellTransition::class);
        $builder->setMethods(['apply']);

        $transition = $builder->getMock();
        $transition
            ->method('apply')
            ->willReturn($this->getNewOrganism());

        return $transition;
    }

    public function testApplyTransition(): void
    {
        $transition = $this->createCellTransitionMock();

        $game = new Game(new God(), $transition, 1);
        $world = $game->run($this->getNewWorld());

        self::assertSame(1, $world->getYear(), 'Age of the world');

        foreach ([
             [1 => 1], [1 => 2], [1 => 3], [1 => 4], [1 => 5],
             [2 => 1], [2 => 5],
             [3 => 1], [3 => 5],
             [4 => 1], [4 => 5],
             [5 => 1], [5 => 2], [5 => 3], [5 => 4], [5 => 5],
        ] as $v) {
            $i = key($v);
            $j = reset($v);

            self::assertNull($world->getOrganismByPosition($i, $j));
        }

        foreach ([
            [2 => 2], [2 => 3], [2 => 4],
            [3 => 2], [3 => 3], [3 => 4],
            [4 => 2], [4 => 3], [4 => 4],
        ] as $v) {
            $i = key($v);
            $j = reset($v);
            $o = $world->getOrganismByPosition($i, $j);

            self::assertInstanceOf(Organism::class, $o);
            self::assertEquals('dog', $o->getType());
        }
    }
}
