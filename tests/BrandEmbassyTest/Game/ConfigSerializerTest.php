<?php
namespace BrandEmbassyTest\Game;

use BrandEmbassy\Game\ConfigSerializer;
use PHPUnit\Framework\TestCase;

class ConfigSerializerTest extends TestCase
{

    public function testBasic(): void
    {
        $xml = $this->getXml();

        $parser = new ConfigSerializer();
        $config = $parser->unserialize($xml);

        $organisms = [
            [
                'i' => 6,
                'j' => 20,
                'type' => 'a',
            ],
            [
                'i' => 2,
                'j' => 3,
                'type' => 'b',
            ],
            [
                'i' => 8,
                'j' => 14,
                'type' => 'a',
            ],
            [
                'i' => 16,
                'j' => 8,
                'type' => 'c',
            ]
        ];

        self::assertEquals(20, $config->getWidth());
        self::assertEquals(20, $config->getHeight());
        self::assertEquals(46272739, $config->getIterations());
        self::assertEquals($organisms, $config->getOrganisms());
    }

    private function getXml(): string
    {
        return <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<life>
    <world>
        <cells>20</cells>
        <species>7</species>
        <iterations>46272739</iterations>
    </world>
    <organisms>
        <organism>
            <x_pos>20</x_pos>
            <y_pos>6</y_pos>
            <species>a</species>
        </organism>
        <organism>
            <x_pos>3</x_pos>
            <y_pos>2</y_pos>
            <species>b</species>
        </organism>
        <organism>
            <x_pos>14</x_pos>
            <y_pos>8</y_pos>
            <species>a</species>
        </organism>
        <organism>
            <x_pos>8</x_pos>
            <y_pos>16</y_pos>
            <species>c</species>
        </organism>
    </organisms>
</life>
EOT;
    }
}
