<?php
namespace Gol\World;

use RuntimeException;

class Garden
{
    /**
     * @var array
     */
    private $garden = [];

    public function addOrganism(Organism $organism): void
    {
        if ($this->hasOrganism($organism)) {
            throw new RuntimeException(sprintf('Organism "%s" already exists', $organism->getType()));
        }

        $this->garden[$organism->getType()] = $organism;
    }

    private function hasType($type): bool
    {
        return isset($this->garden[$type]);
    }

    public function hasOrganism(Organism $organism): bool
    {
        return $this->hasType($organism->getType());
    }

    public function getOrganism(string $type): Organism
    {
        if (!$this->garden[$type]) {
            throw new RuntimeException(sprintf('Organism "%s" not exists', $type));
        }

        return $this->garden[$type];
    }
}
