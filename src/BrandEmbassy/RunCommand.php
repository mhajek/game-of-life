<?php
namespace BrandEmbassy;

use BrandEmbassy\Game\ConfigSerializer;
use BrandEmbassy\Game\Transition;
use Gol\Game\Config;
use Gol\Game\Game;
use Gol\World\God;
use Gol\World\Organism;
use Gol\World\Garden;
use Gol\World\World;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Exception;
use RuntimeException;

class RunCommand extends Command
{
    private const ARGUMENT_FILE = 'file';

    public function __construct()
    {
        parent::__construct('be:run');

        $this->setDescription('Run!');
        $this->addArgument(self::ARGUMENT_FILE, InputArgument::REQUIRED, 'Configuration file path (*.xml)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $file = $input->getArgument(self::ARGUMENT_FILE);
        if (!file_exists($file)) {
            throw new RuntimeException(sprintf('Configuration file %s not exist', $file));
        }

        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        if ($ext !== 'xml') {
            throw new RuntimeException(sprintf('Unsupported %s configuration', strtoupper($ext)));
        }

        $startTime = microtime(true);

        $serializer = new ConfigSerializer();
        $config = $serializer->unserialize(file_get_contents($file));

        $garden = $this->createGarden($config);

        $god = new God();
        $word = $god->createWorld($config->getWidth(), $config->getHeight());

        foreach ($config->getOrganisms() as $data) {
            $i = $data['i'];
            $j = $data['j'];
            $organism = $garden->getOrganism($data['type']);
            $current = $word->getOrganismByPosition($i, $j);

            if ($current !== null) {
                $array = [$organism, $current];
                $key = array_rand($array);
                $organism = $array[$key];
            }

            $word->addOrganismToPosition($i, $j, $organism);
        }

        $game = new Game($god, new Transition(), $config->getIterations());

        try {
            $lastWorld = $game->run($word);
        } catch (Exception $e) {
            $output->writeln((string)$e);
            return;
        }

        $resultConfig = $this->saveToConfig($lastWorld);
        $xml = $serializer->serialize($resultConfig);

        $output->writeln($xml);
        $output->writeln(sprintf('%1.3fMB', memory_get_usage() / 1024 / 1024));
        $output->writeln(sprintf('%1.3fs', microtime(true) - $startTime));
    }

    private function createGarden(Config $config): Garden
    {
        $garden = new Garden();
        foreach ($config->getOrganisms() as $data) {
            $organism = new Organism($data['type']);

            if (!$garden->hasOrganism($organism)) {
                $garden->addOrganism($organism);
            }
        }

        return $garden;
    }

    private function saveToConfig(World $world): Config
    {
        $height = $world->getHeight();
        $width = $world->getWidth();
        $organisms = [];

        for ($i = 1; $i <= $height; $i++) {
            for ($j = 1; $j <= $width; $j++) {
                $organism = $world->getOrganismByPosition($i, $j);
                if ($organism === null) {
                    continue;
                }

                $organisms[] = [
                    'i' => $i,
                    'j' => $j,
                    'type' => $organism->getType(),
                ];
            }
        }

        return new Config($width, $height, $world->getYear(), $organisms);
    }
}
