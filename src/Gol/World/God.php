<?php
namespace Gol\World;

class God implements WorldCreator
{
    public function createWorld(int $height, int $width): World
    {
        return new World($height, $width);
    }
}
