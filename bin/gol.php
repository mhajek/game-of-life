#!/usr/bin/env php
<?php
require_once dirname(__DIR__) . '/vendor/autoload.php';

$app = new Symfony\Component\Console\Application('Game Of Life');
$app->add(new BrandEmbassy\RunCommand());
$app->run();
