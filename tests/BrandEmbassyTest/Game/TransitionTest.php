<?php
namespace BrandEmbassyTest\Game;

use BrandEmbassy\Game\Transition;
use Gol\World\World;
use Gol\World\Organism;
use PHPUnit\Framework\TestCase;

class TransitionTest extends TestCase
{
    /**
     * @var World
     */
    private $world;

    protected function setUp(): void
    {
        $this->world = new World(3, 3);
    }

    /**
     * If are 2 organisms
     */
    public function testTransitionRule12(): void
    {
        $o1 = new Organism('mouse');

        $this->world->addOrganismToPosition(1, 3, $o1);
        $this->world->addOrganismToPosition(2, 2, $o1);
        $this->world->addOrganismToPosition(3, 3, $o1);

        $transition = new Transition();
        $organism = $transition->apply($this->world, 2, 2);

        self::assertEquals($o1, $organism);
    }

    public function testTransitionRule12Fail(): void
    {
        $o1 = new Organism('mouse');

        $this->world->addOrganismToPosition(2, 2, $o1);
        $this->world->addOrganismToPosition(3, 3, $o1);

        $transition = new Transition();
        $organism = $transition->apply($this->world, 2, 2);

        self::assertEquals(null, $organism);
    }

    /**
     * If are there 3
     */
    public function testTransitionRule13(): void
    {
        $o1 = new Organism('mouse');

        $this->world->addOrganismToPosition(1, 3, $o1);
        $this->world->addOrganismToPosition(2, 2, $o1);
        $this->world->addOrganismToPosition(2, 3, $o1);
        $this->world->addOrganismToPosition(3, 3, $o1);

        $transition = new Transition();
        $organism = $transition->apply($this->world, 2, 2);

        self::assertEquals($o1, $organism);
    }

    /**
     * If are there less 2
     */
    public function testTransitionRule2Isolation(): void
    {
        $o1 = new Organism('mouse');

        $this->world->addOrganismToPosition(1, 3, $o1);
        $this->world->addOrganismToPosition(2, 2, $o1);

        $transition = new Transition();
        $organism = $transition->apply($this->world, 2, 2);

        self::assertEquals(null, $organism);
    }

    /**
     * If are there 4
     */
    public function testTransitionRule4Born(): void
    {
        $o1 = new Organism('mouse');
        $o2 = new Organism('cat');

        $this->world->addOrganismToPosition(1, 1, $o2);
        $this->world->addOrganismToPosition(1, 2, $o2);
        $this->world->addOrganismToPosition(1, 3, $o1);
        $this->world->addOrganismToPosition(2, 1, $o2);
        $this->world->addOrganismToPosition(2, 3, $o1);
        $this->world->addOrganismToPosition(3, 1, $o2);
        $this->world->addOrganismToPosition(3, 2, $o2);
        $this->world->addOrganismToPosition(3, 3, $o1);

        $transition = new Transition();
        $organism = $transition->apply($this->world, 2, 2);

        self::assertSame(spl_object_hash($o1), spl_object_hash($organism));
    }

    /**
     * If are there 4, more than one species
     */
    public function testTransitionRule4BornTwice(): void
    {
        $o1 = new Organism('mouse');
        $o2 = new Organism('cat');

        $this->world->addOrganismToPosition(1, 1, $o1);
        $this->world->addOrganismToPosition(1, 2, $o1);
        $this->world->addOrganismToPosition(1, 3, $o1);
        $this->world->addOrganismToPosition(3, 1, $o2);
        $this->world->addOrganismToPosition(3, 2, $o2);
        $this->world->addOrganismToPosition(3, 3, $o2);

        $transition = new Transition();
        $organism = $transition->apply($this->world, 2, 2);

        self::assertTrue(in_array(spl_object_hash($organism), [spl_object_hash($o1), spl_object_hash($o2)], true));
    }
}
