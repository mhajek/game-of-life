<?php
namespace Gol\World;

use InvalidArgumentException;
use RuntimeException;

class World
{
    /**
     * @var int
     */
    private $height;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $year = 0;

    /**
     * @var array
     */
    private $matrix = [];

    public function __construct(int $height, int $width)
    {
        if ($height < 1) {
            throw new InvalidArgumentException(sprintf('Invalid 1 parameter; %s given', $height));
        }
        if ($width < 1) {
            throw new InvalidArgumentException(sprintf('Invalid 2 parameter; %s given', $width));
        }

        $this->height = $height;
        $this->width = $width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    private function getOffsetByPosition(int $i, int $j): int
    {
        return $this->width * ($i - 1) + $j - 1;
    }

    /**
     * @param int $i Row
     * @param int $j Column
     * @param Organism|NULL $organism
     */
    public function addOrganismToPosition(int $i, int $j, Organism $organism = null): void
    {
        if (!$this->positionExists($i, $j)) {
            throw new RuntimeException(
                sprintf(
                    'Coordinates [%d, %d] are out of range; [%d, %d]',
                    $i,
                    $j,
                    $this->width,
                    $this->height
                )
            );
        }

        $offset = $this->getOffsetByPosition($i, $j);
        $this->matrix[$offset] = $organism;
    }

    public function getOrganismByPosition(int $i, int $j): ?Organism
    {
        $offset = $this->getOffsetByPosition($i, $j);

        return $this->matrix[$offset] ?? null;
    }

    private function positionExists(int $i, int $j): bool
    {
        return $i <= $this->height && $i > 0 && $j <= $this->width && $j > 0;
    }

    /**
     * @param int $i
     * @param int $j
     * @return Organism[] Array [1..9]
     */
    public function getOrganismsAroundPosition(int $i, int $j): array
    {
        $return = [
            1 => null,
            2 => null,
            3 => null,
            4 => null,
            6 => null,
            7 => null,
            8 => null,
            9 => null,
        ];

        // 1 row
        if ($this->positionExists($i - 1, $j - 1)) {
            $return[1] = $this->getOrganismByPosition($i - 1, $j - 1);
        }
        if ($this->positionExists($i - 1, $j)) {
            $return[2] = $this->getOrganismByPosition($i - 1, $j);
        }
        if ($this->positionExists($i - 1, $j + 1)) {
            $return[3] = $this->getOrganismByPosition($i - 1, $j + 1);
        }
        // 2 row
        if ($this->positionExists($i, $j - 1)) {
            $return[4] = $this->getOrganismByPosition($i, $j - 1);
        }
        if ($this->positionExists($i, $j + 1)) {
            $return[6] = $this->getOrganismByPosition($i, $j + 1);
        }
        // 3 row
        if ($this->positionExists($i + 1, $j - 1)) {
            $return[7] = $this->getOrganismByPosition($i + 1, $j - 1);
        }
        if ($this->positionExists($i + 1, $j)) {
            $return[8] = $this->getOrganismByPosition($i + 1, $j);
        }
        if ($this->positionExists($i + 1, $j + 1)) {
            $return[9] = $this->getOrganismByPosition($i + 1, $j + 1);
        }

        return $return;
    }
}
