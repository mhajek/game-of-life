<?php
namespace BrandEmbassy\Game;

use Gol\Game\Config;
use XMLWriter;
use SimpleXMLElement;

class ConfigSerializer
{

    public function serialize(Config $config): string
    {
        $types = [];
        foreach ($config->getOrganisms() as $organism) {
            $types[] = $organism['type'];
        }

        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->startDocument('1.0', 'UTF-8');
        $xmlWriter->startElement('life');
        $xmlWriter->startElement('world');
        $xmlWriter->writeElement('cells', $config->getHeight());
        $xmlWriter->writeElement('species', count(array_unique($types)));
        $xmlWriter->writeElement('iterations', $config->getIterations());
        $xmlWriter->endElement();

        $xmlWriter->startElement('organisms');
        foreach ($config->getOrganisms() as $organism) {
            /** @noinspection DisconnectedForeachInstructionInspection */
            $xmlWriter->startElement('organism');
            $xmlWriter->writeElement('x_pos', $organism['j']);
            $xmlWriter->writeElement('y_pos', $organism['i']);
            $xmlWriter->writeElement('species', $organism['type']);
            /** @noinspection DisconnectedForeachInstructionInspection */
            $xmlWriter->endElement();
        }
        $xmlWriter->endElement();

        $xmlWriter->endElement();
        $xmlWriter->endDocument();

        return $xmlWriter->outputMemory();
    }

    public function unserialize($string): Config
    {
        $xml = new SimpleXMLElement($string);

        $size = (int)$xml->world->cells;
        $iterations = (int)$xml->world->iterations;
        $organisms = [];

        foreach ($xml->organisms->organism as $o) {
            $organisms[] = [
                'i' => (int)$o->y_pos,
                'j' => (int)$o->x_pos,
                'type' => (string)$o->species,
            ];
        }

        return new Config($size, $size, $iterations, $organisms);
    }
}
